/**
* Created by teisaacs on 5/29/14.
*/

var app = angular.module('devWorkshop.Module1', ['ngRoute']);


app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/module1', {templateUrl: 'src/module1/module1.tpl.html', controller: 'Module1Ctrl'} );
}]);


app.controller('Module1Ctrl', function($scope) {
    $scope.showMsg = function(msg) {
        alert(msg);
    }
});

