/**
 * Application file defines the dependencies of the application and sets the default routes.
 *
 * Created by todd shelton on 6/28/14.
 */
var app = angular.module('dwConfApp', [
    'ngRoute',
    'devWorkshop.NavModule',
    'devWorkshop.Module1'
]);

//app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider){
//    $routeProvider.when('/home', {
//        templateUrl: 'partials/home.html',
//        controller: 'HomeCtrl'
//    });
//    $routeProvider.when('/about', {
//        templateUrl: 'partials/about.html',
//        controller: 'AboutCtrl'
//    });
//}]);


