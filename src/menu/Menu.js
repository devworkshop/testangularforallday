/**
* Created by todd shelton on 6/28/14.
*/

var nav = angular.module('devWorkshop.NavModule',[
    'ngRoute'
]);

nav.controller('NavCtrl', function($scope, $location){

    $scope.changeNav = function(path){
        $location.path( path );
    }
});


nav.config(['$routeProvider', '$locationProvider', function ($routeProvider){
    $routeProvider.when('/home', {templateUrl: 'src/views/home/home.tpl.html'});
    $routeProvider.when('/', {templateUrl: 'src/views/home/home.tpl.html'});
    $routeProvider.when('/work', {templateUrl: 'src/views/work/work.tpl.html'});
    $routeProvider.when('/contact', {templateUrl: 'src/views/contact/contact.tpl.html'});
//    $routeProvider.otherwise({redirectTo: '/home'});

    $routeProvider.when('/about', {
        templateUrl: 'src/views/about/about.tpl.html',
    });
}]);
